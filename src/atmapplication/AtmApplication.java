package atmapplication;

import com.thoughtworks.xstream.XStream;
import java.io.*;
import java.util.*;

/**
 * This class runs the ATM application. Contains the main method, where an instance of the class is 
 * always created.
 * Library: com.thoughworks.xstream.XStream
 * XStream - Copyright (c) 2003-2006, Joe Walnes Copyright (c) 
 * 2006-2009, 2011 XStream Committers. All rights reserved.
 * @author rafaelbattesti
 * @version 1.0
 * @since 2015-05-19
 */
public class AtmApplication {
    
    /**
     * Constant that holds the string values for the main menu.
     */
    private final String[] ACCOUNT_MENU = {"Check Balance", "Withdraw", "Deposit", "Exit"};
    
    /**
     * Holds a Bank instance, which in turn contains an ArrayList of accounts.
     */
    private Bank _bank;
    
    /**
     * Holds the active account during a session.
     */
    private Account _activeAccount;
    
    /**
     * Main Method instantiates AtmApplication and calls run().
     * @param args[] List of command line arguments.
     */
    public static void main(String[] args) {
        
        //Instantiate app and run the application
        AtmApplication app = new AtmApplication();
        app.run();
                   
    }
    
    /**
     * Runs the application. Called from the instance of AtmApplication at the main method.
     */
    private void run() {
        
        //Instantiates a new Bank and writes the accounts created to "bank.xml"
        _bank = new Bank();
        writeToXml();
        
        //Keeps the program running in a loop unless ATM switched off (outer loop).
        while (true) {
            
            //Prints header, calls login(), welcomes user, updates last login and writes to XML.
            initSystem();
            
            //Keeps the current session running unless user chooses to exit (inner loop).
            loop:
            while (true) {
                
                //Prints menu.
                initMenu();
              
                //ATM operation
                switch(validateMenuInput()) {
                    case 1:
                        delayOutput("\nRetrieving balance ");
                        System.out.printf("\nYour current balance is: $%,.2f\n\n",
                                _activeAccount.getBalance());
                        break;
                        
                    case 2:
                        runWithdraw();
                        writeToXml();
                        break;
                        
                    case 3:
                        runDeposit();
                        writeToXml();
                        break;
                        
                    case 4:
                        System.out.println("\nThank you for your custom. Goodbye :-)\n");
                        break loop;
                }   
            }
        }
    }
    
    /**
     * Prints system header, calls login(), greets user and writes to XML.
     */
    private void initSystem() {
        
        // Prints the welcoming header
        System.out.println("--------- Welcome to Scotia Bank Canada ---------");
        printLine();

        /* userLogin validates account# input and assigns _activeAccount */
        login();

        // Prints user greetings
        System.out.println();
        System.out.printf("Welcome Mr. %s %s!\n", _activeAccount.getFirstName(),
                _activeAccount.getLastName());
        System.out.printf("%s %s\n", "Client since:", _activeAccount.getDateCreated());

        // Prints last login ONLY if user has accessed before (not null).
        if (_activeAccount.getLastLogin() != null) {
            System.out.printf("%-13s %s\n", "Last Login:", _activeAccount.getLastLogin());
        }
        
        // _lastLogin = _currentSession.
        _activeAccount.setLastLogin(new Date());

        // Writes to xml on initialization to avoid information loss.
        writeToXml();
    }
    
    /**
     * Validates the user input for the account number (login). Asks for input
     * until the user enters a valid account#.
     */
    private void login() {
        
        Scanner input = new Scanner(System.in);
        
        // Keep asking for input until user does it correctly.
        while (true) {
            
            System.out.print("Account #: ");

            // Handles Exceptions for account# input.
            try {

                //Prompts user for input. Throws NumberFormatException or InvalidArgumentExcpetion
                _activeAccount = _bank. retrieveAccount(Integer.parseInt(input.nextLine()));
                break;

            } catch (NumberFormatException e) {
                
                //Prints message if the input is not valid for the operation
                printLine();
                System.out.println("Could not read the card, please try again.");
            }
        }
        
        //Delays output for a "processing" feeling
        delayOutput("\nRetrieving account ");
 
    }
    
    /**
     * Writes the _accountList to "bank.xml". It is called in initSystem() to update the 
     * _lastLogin and at every modification to the account (withdrawal or deposit).
     */
    private void writeToXml () {
        
        // Generates the xml string for this Bank instance.
        XStream xstream = new XStream();
        String xml = xstream.toXML(_bank.getAccountList());
        
        // Saves xml string to file
        try {
            
            // Creates a new file object
            File file = new File("bank.xml");
            
            // If the file object does not exist in the path, create a new file.
            if (!file.exists()) {
                file.createNewFile();
            }
            
            // Writes to xml file using a buffered file writer
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(xml);
            bw.close();
            
            
        } catch (IOException e) {
            
            System.out.println("Error: " + e.getMessage());
        } 
    }
    
    /**
     * Prints the main menu.
     */
    private void initMenu() {

        // Prints the Main Menu
        System.out.println("------------------- Main menu -------------------");
        printLine();
        for (int i = 0; i < ACCOUNT_MENU.length; i++) {
            System.out.printf("%d. %s\n", (i + 1), ACCOUNT_MENU[i]);
        }
    }
    
    /**
     * Validates the main menu input.
     * @return option chosen by the user.
     */
    private int validateMenuInput() {
        
        Scanner input = new Scanner(System.in);
        
        // Holds the option chosen by the user
        int option;
        
        // Keep asking for input until user does it correctly.
        while(true) {
            
            printLine();
            System.out.print("Please enter your option: ");
            
            // Validates input for option.
            try {
                
                //Prompts user for input. Throws a NumberFormatException.
                option = Integer.parseInt(input.nextLine());
                
                // If option is not valid, throw InvalidArgumentException
                if (option < 1 || option > 4) {
                    throw new InvalidArgumentException();
                }
                
                // If input is acceptable, break the loop.
                break;
                
            } catch (NumberFormatException e) {
                
                //Prints message if the input is not valid for the operation
                System.out.println("Not a valid option, please try again.");
            }
        }
        
        // If everything went fine, return the chosen option.
        return option;  
    }
    
    /**
     * Validates the strict use of bills by ATM (amount must be divisible by 5).
     * @return cashAmount validated to be withdrawn or deposited.
     * @throws atmapplication.InvalidArgumentException If the amount is not divisible by 5 - 
     * ATM only operates.
     */
    private int validateCashInput() throws InvalidArgumentException, NumberFormatException {

        Scanner input = new Scanner(System.in);
        int cashAmount;

        cashAmount = Integer.parseInt(input.nextLine());

        if (cashAmount % 5 != 0) {
            throw new InvalidArgumentException();
        }
        
        return cashAmount;
    }
    
    /**
     * Runs withdraw(double amount) from account and is called at run().
     */
    private void runWithdraw() {

        while (true) {
            
            //Prompts the user with informative messages for input
            System.out.println("Only 100, 50, 20, 10 and 5 bills available.");
            System.out.print("Cash amount to withdraw: $");

            try {
                
                //Gets input from the user and performs the withdraw operation
                _activeAccount.withdraw(validateCashInput());
                
                //If input gets validated and operation is valid, move on and leave the loop
                delayOutput("\nCounting bills ");
                System.out.println("\nWithdraw successful, please take your money.");
                System.out.printf("Your new balance is: $%,.2f\n\n", _activeAccount.getBalance());
                break;
                
            } catch (NotEnoughFundsException e) {
                
                //Print a message if the user does not have funds and cancel operation
                printLine();
                System.out.println(e.getMessage());
                delayOutput("Canceling operation ");
                System.out.println();
                break;
                
            } catch (NumberFormatException e) {
                
                //Prints message if the input is not valid for the operation
                printLine();
                System.out.println("Not a valid amount, please try again.");
            }
        }
    }
    
    /**
     * Calls deposit(double amount) from account and is called at run().
     */
    private void runDeposit() {

        while (true) {
            
            //Prompts the user with informative messages for input
            System.out.println("Only 100, 50, 20, 10 and 5 bills accepted.");
            System.out.print("Cash amount to deposit: $");
            
            try {
                
                //Gets input from the user and performs the deposit operation
                _activeAccount.deposit(validateCashInput());
                break;
                
            } catch (NumberFormatException e) {
                
               //Prints message if the input is not valid for the operation
                printLine();
                System.out.println("Not a valid amount, please try again.");
            }
        }
        
        //If input gets validated and operation is valid, move on
        delayOutput("\nProcessing deposit ");
        System.out.println("\nDeposit successful, please take your receipt.");
        System.out.printf("Your new balance is: $%,.2f\n\n", _activeAccount.getBalance());
    }
    
    /**
     * Prints a fancy loading bar :-)
     * @param task The task that has been performed, as a label to be printed out to the UI.
     */
    private void delayOutput(String task) {
        System.out.print(task);
        try {
            for (int i = 0; i < 5; i++) {
                Thread.sleep(500);
                System.out.print(".");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println();
    }
    
    /**
     * Prints the separator line
     */
    private void printLine() {
        for (int i = 0; i < 49; i++) {
            System.out.print("-");
        }
        System.out.println();
    }
}
