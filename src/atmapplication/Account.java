package atmapplication;

import java.util.Date;

/**
 * This class models the Account object. An account holds a series of properties and accessors and 
 * modifiers to those properties. Third party libraries include:
 * Library - com.thoughworks.xstream.XStream
 * XStream - Copyright (c) 2003-2006, Joe Walnes Copyright (c) 
 * 2006-2009, 2011 XStream Committers. All rights reserved.
 * @author rafaelbattesti
 * @version 1.0
 * @since 2015-05-19
 */
public class Account {
    
    protected String _firstName;
    protected String _lastName;
    protected int    _id;
    protected double _balance;
    protected double _annualInterestRate;
    protected Date   _dateCreated;
    protected Date   _lastLogin;

    public Account(int id, String firstName, String lastName, double balance) {

        // Initializes the other data fields
        _id        = id;
        _firstName = firstName;
        _lastName  = lastName;
        _balance   = balance;
        _dateCreated = new Date();
    }
    
    /**
     * Accesses Account _id.
     * @return _id - The Account #.
     */
    public int getId() {
        return _id;
    }

    /**
     * Accesses Account holder _firstName. 
     * @return _firsName
     */
    public String getFirstName() {
        return _firstName;
    }

    /**
     * Accesses Account holder _lastName.
     * @return  _lastName
     */
    public String getLastName() {
        return _lastName;
    }

    /**
     * Accesses Account _balance.
     * @return _balance
     */
    public double getBalance() {
        return _balance;
    }

    /**
     * Sets a new Account _balance.
     * @param balance The balance to be set.
     */
    public void setBalance(double balance) {
        _balance = balance;
    }
    
    /**
     * Accesses Account _annualInterestRate.
     * @return _annualInterestRate
     */
    public double getAnnualInterestRate() {
        return _annualInterestRate;
    }
    
    /**
     * Sets Account _annualInterest.
     * @param annualInterestRate The interest rate to be set.
     * @throws InvalidArgumentException If annualInterestRate is negative.
     */
    public void setAnnualInterestRate(double annualInterestRate) throws InvalidArgumentException {
        
        if (annualInterestRate >= 0) {
            _annualInterestRate = annualInterestRate;
        } else {
            throw new InvalidArgumentException();
        }

    }
    
    /**
     * Accesses Account _dateCreated.
     * @return _dateCreated
     */
    public Date getDateCreated() {
        return _dateCreated;
    }
    
    /**
     * Accesses the user _lastLogin.
     * @return _lastLogin
     */
    public Date getLastLogin() {
        return _lastLogin;
    }
    
    /**
     * Sets user _lastLogin.
     * @param lastLogin The Date the user last logged in.
     */
    public void setLastLogin(Date lastLogin) {
        _lastLogin = lastLogin;
    }
    
    /**
     * Calculates the monthly interest rate based on the _annualInterestRate.
     * @return the monthly interest rate
     */
    public double getMonthlyInterestRate() {
        return _annualInterestRate / 12;
    }
    
    /**
     * Calculates the monthly interest in dollars based on Account balance.
     * @return the amount of interest owed in a month.
     */
    public double getMonthlyInterest() {
        return _balance * getMonthlyInterestRate();
    }
    
    /**
     * Withdraws money from the ATM. Verify funds and deduct.
     * @param amount to deduct.
     * @return The updated account balance.
     * @throws NotEnoughFundsException for values bigger than the balance
     * @throws InvalidArgumentException for all other wrong inputs.
     */
    public double withdraw(double amount) throws NotEnoughFundsException, InvalidArgumentException {
        
        if (amount > _balance) {
            throw new NotEnoughFundsException("Sorry, not enough funds :-(");
        } else if (amount <= 0) {
            throw new InvalidArgumentException();
        } else {
            return _balance -= amount; 
        }
    }
    
    /**
     * Makes a deposit. Adds the value deposited into account.
     * @param amount to add.
     * @return The updated account balance.
     * @throws InvalidArgumentException for 0 and negative values
     */
    public double deposit(double amount) throws InvalidArgumentException {
        
        if (amount <= 0) {
            throw new InvalidArgumentException();
        } else {
            return _balance += amount;  
        } 
        
    }  
}
