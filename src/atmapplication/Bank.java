package atmapplication;

import java.io.*;
import java.util.ArrayList;
import com.thoughtworks.xstream.XStream;

/**
 * This class models the Bank object. A bank holds an ArrayList of Accounts and the methods to 
 * retrieve an account and to read data from "bank.xml".
 * com.thoughworks.xstream.XStream
 * XStream - Copyright (c) 2003-2006, Joe Walnes Copyright (c) 
 * 2006-2009, 2011 XStream Committers. All rights reserved.
 * @author rafaelbattesti
 * @version 1.0
 * @since 2015-05-19
 */
public class Bank {
    
    /**
     * This constant contains the data to generate the accounts when the user first runs this 
     * application. After the first run, each account gets a _dateOfCreation and is saved to file
     * "bank.xml"
     */
    private final String[][] ACCOUNT_DATA = {
        {"Ronaldo", "Nazario", "100.00"},
        {"Edson", "Nascimento", "100.00"},
        {"Romário", "Faria", "100.00"},
        {"Michael", "Jordan", "100.00"},
        {"Gheorghe", "Hagi", "100.00"},
        {"Kareen", "Jabar", "100.00"},
        {"Scott", "Pippen", "100.00"},
        {"Vince", "Carter", "100.00"},
        {"Roger", "Federer", "100.00"},
        {"Rafael", "Nadal", "74834792384.14"}
    };
    
    /**
     * Holds the list of Accounts available in the Bank.
     */
    private ArrayList<Account> _accountList = new ArrayList<>();
    
    /**
     * Looks for a "bank.xml" file to decide how to instantiate
     * a bank object. If file does not exist, it creates the accounts with the values from 
     * ACCOUNT_DATA. Otherwise, it reads "bank.xml" and uses XStream to assign values to each 
     * account field.
     */
    public Bank() {
        
        //File object to verify the existance of a file in the relative path.
        File file = new File("bank.xml");
        
        //If the file does not exist, create accounts from constant values.
        if (!file.exists()) {
            
            //Create accounts in the loop with values from the constant.
            for (int i = 0; i < 10; i++) {
            
                Account account = new Account(i + 1, ACCOUNT_DATA[i][0], ACCOUNT_DATA[i][1], 
                        Double.parseDouble(ACCOUNT_DATA[i][2]));
                _accountList.add(account);
            }  
            
        } else {
            
            // If the file exists, then read it to create the bank accounts.
            _accountList = (ArrayList<Account>)readFromXml();   
        }
    }
    
    /**
     * Accesses Bank _accountList.
     * @return ArrayList of Accounts
     */
    public ArrayList getAccountList () {
        return _accountList;
    }
    
    /**
     * Retrieves Account in _accountList by id.
     * @param accountId The account id to be searched for.
     * @return The Account to be the _activeAccount on the AtmApplication
     * @throws atmapplication.InvalidArgumentException when account is not found
     */
    public Account retrieveAccount (int accountId) throws
            InvalidArgumentException {
        
        //Iterates across the array
        for (int i = 0; i < _accountList.size(); i++) {
            
            //Returns the account if getId() matches accountId
            if (_accountList.get(i).getId() == accountId) {
                return _accountList.get(i);
            }
        }
        
        //Otherwise, throws InvalidArgumentException
        throw new InvalidArgumentException();
    }
    
    /**
     * Reads "bank.xml" and parses its values to accounts fields. Called in the Bank() constructor.
     * @return ArrayList of Accounts.
     */
    private ArrayList readFromXml() {
        
        // lines will be concatenated to form the file
        String line;
        String xml = new String();
        XStream xstream = new XStream();
        xstream.addImplicitCollection(Bank.class, "_accountList");
        
        try {
            
            // Creates the file reader
            FileReader fr = new FileReader("bank.xml");
            
            // Creates the reader buffer
            BufferedReader br = new BufferedReader(fr);
            
            // Reads a line and concatenates with the previous
            do {
                line = br.readLine();    
                xml += line;
            } while (line != null);
            
            
        } catch (FileNotFoundException e) {
            
            //Informs the user in case it cannot find the file
            System.out.println("File not found!");
            
        } catch (Exception e) {
            
            //Informs the user in case it any errors happen when reading file
            System.out.println("Error reading file!");
            
        }
        
        //Returns the ArrayList parsed from the xml string
        return (ArrayList<Account>) xstream.fromXML(xml);
    }
    
}
