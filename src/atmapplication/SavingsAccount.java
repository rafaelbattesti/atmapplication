/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmapplication;

/**
 *
 * @author rafaelbattesti
 */
public class SavingsAccount extends Account {

    public SavingsAccount(int id, String firstName, String lastName, double balance, 
            double annualInterestRate) {
        
        super(id, firstName, lastName, balance);
        this.setAnnualInterestRate(annualInterestRate);
    }
    
    /**
     * Sets Account _annualInterest.
     * @param annualInterestRate The interest rate to be set.
     * @throws InvalidArgumentException If annualInterestRate is negative or higher than 3%.
     */
    @Override
    public void setAnnualInterestRate(double annualInterestRate) throws InvalidArgumentException {
        
        if (annualInterestRate >= 0 && annualInterestRate <= 3) {
            _annualInterestRate = annualInterestRate;
        } else {
            throw new InvalidArgumentException();
        }

    }
    
}
