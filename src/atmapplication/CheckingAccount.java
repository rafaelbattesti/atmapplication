/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atmapplication;

/**
 *
 * @author rafaelbattesti
 */
public class CheckingAccount extends Account {
    
    public CheckingAccount(int id, String firstName, String lastName, double balance, 
            double annualInterestRate) {
        
        super(id, firstName, lastName, balance);
        this.setAnnualInterestRate(annualInterestRate);
    }
    
    /**
     * Withdraws money from the ATM. Verify funds and deduct.
     * @param amount to deduct.
     * @return The updated CheckingAccount balance.
     * @throws NotEnoughFundsException for values exceeding 500 overdraft.
     * @throws InvalidArgumentException for all other wrong inputs.
     */
    @Override
    public double withdraw(double amount) throws NotEnoughFundsException, InvalidArgumentException {
        
        if (amount > getBalance() - 500) {
            throw new NotEnoughFundsException("Sorry, not enough funds :-(");
        } else if (amount <= 0) {
            throw new InvalidArgumentException();
        } else {
            return _balance -= amount; 
        }
    }
    
    /**
     * Sets CheckingAccount _annualInterest.
     * @param annualInterestRate The interest rate to be set.
     * @throws InvalidArgumentException If annualInterestRate is negative or higher than 1%.
     */
    @Override
    public void setAnnualInterestRate(double annualInterestRate) throws InvalidArgumentException {
        
        if (annualInterestRate >= 0 && annualInterestRate <= 1) {
            _annualInterestRate = annualInterestRate;
        } else {
            throw new InvalidArgumentException();
        }

    }
    
}
