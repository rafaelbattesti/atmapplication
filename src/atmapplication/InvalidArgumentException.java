package atmapplication;

/**
 * Exception class made as a child of NumberFormatExcpetion to make it easier
 * to handle both in the same catch block.
 * @author rafaelbattesti
 */
public class InvalidArgumentException extends NumberFormatException {
    
    String _message;
    
    public InvalidArgumentException() {
        super();
    }

    public InvalidArgumentException(String message) {
        super(message);
    }
}