package atmapplication;

/**
 * Exception class that calls to handle invalid withdraw amount (bigger than balance).
 * @author rafaelbattesti
 */
public class NotEnoughFundsException extends Exception {
    
    public NotEnoughFundsException() {
        super();
    }

    public NotEnoughFundsException(String message) {
        super(message);
    }
}
